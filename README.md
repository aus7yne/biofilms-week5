# BIOFILMS RENTAL

## A simple film rental business website that has the following features:

1. Has a header, that displays the company logo, and navigation links,
2. Has a full background image related to the company, 
3. Has a little information about the company

## Live_Page

To test and review the live page for the **FILM RENTAL WEBSITE** please click [here](https://biofilms-week5.herokuapp.com/).
